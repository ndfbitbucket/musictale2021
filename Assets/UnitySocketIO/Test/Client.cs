using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySocketIO;
using UnitySocketIO.Events;
using System;
using RhythmGameStarter;
using System.IO;
public class Client : MonoBehaviour
{
    public SocketIOController io;
    public MymidiImporter songlistObj;
    public string stat;
    public List<string[]> missed;
    public GameObject Backgourd;
    // public List<string> name;

    ResultManager resultManager;
    public static Client Instance { get; private set; }
    private void Awake()
    {


        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    [System.Serializable]
    public class emptyjson
    {
        public bool a;
    }
    public string EMPTYJSON;
    // Start is called before the first frame update
    void Start()
    {
        var a = new emptyjson();
        EMPTYJSON = JsonUtility.ToJson(a);
        io.On("connect", e =>
        {

        });

        io.On("disconnect", e =>
        {

        });

        io.On("playMusic", e =>
        {
            Debug.Log("Play Student ");
            var myObject = JsonUtility.FromJson<song_req>(e.data);
            MymidiImporter songlist = songlistObj.GetComponent<MymidiImporter>();
            if (stat == "Student")
            {
                int c = Convert.ToInt32(myObject.count);
                Debug.Log(c);
                Debug.Log(myObject.song);
                Debug.Log(FindPath(myObject.song, "L"));
                Debug.Log(songlist.list_L);
                Debug.Log(songlist.list_R);
                Debug.Log(songlist.list_LR);

                if (c==1)
                {
                    songlist.ClickPlay(FindPath(myObject.song, "L"), FindSongItem(myObject.song, "L"));
                }
                if (c==0)
                {
                    songlist.ClickPlay(FindPath(myObject.song, "R"), FindSongItem(myObject.song, "R"));
                }
                if (c == 3)
                {
                    songlist.ClickPlay(FindPath(myObject.song, "LR"), FindSongItem(myObject.song, "LR"));
                }

            }

        });

        io.On("endSong", e =>
        {
            var result = JsonUtility.FromJson<endsong_req>(e.data);
            if (stat == "teacher")
            {
                if (resultManager == null)
                    resultManager = GameObject.FindObjectOfType<ResultManager>();

                resultManager.list.Add(result);
                Debug.Log(result.username);

            }
            Debug.Log(e.data);

        });

        io.On("updateClients", e =>
        {
            print("updateClients");
            print(e.data);
            //Debug.Log(JsonUtility.FromJson<clients_req>(e).username);
        });

        io.Connect();
    }

    public void Login(string username, Action<bool> callback)
    {
        login_req req = new login_req();
        req.username = username;

        print("Login Press");

        io.Emit("login", req.toJson(), e =>
          {
              callback.Invoke(JsonUtility.FromJson<login_req>(e).logined);
              print(e);
          });
    }

    public void Joinroom(string room, Action<bool> callback)
    {
        room_req req = new room_req();
        req.room = room;

        print("Join Room Press");

        io.Emit("joinroom", req.toJson(), e =>
        {
            //JoinRoom Done
            callback.Invoke(JsonUtility.FromJson<room_req>(e).joined);
            print(e);
        });
    }

    public void TestJoinroom()
    {
        Joinroom("A", logined =>
        {
            if (logined)
            {
                //Login Done
            }
        });
    }

    public void TestLogin()
    {
        Login("���A", joined =>
        {
            if (joined)
            {
                //Loadscene
            }
        });
    }

    public void PlayMusic(string songname, int count)
    {
        Debug.Log("PlayMusic ReQ To S");
        song_req req = new song_req();
        req.song = songname;
        req.count = count;

        Debug.Log(req.song);
        Debug.Log(req.count);
        io.Emit("playMusic", req.toJson());
        Debug.Log("After Emit");



    }

    public void EndSong(string song, float score, string[] listPersanMissed)
    {
        endsong_req req = new endsong_req();
        req.song = song;
        req.score = score;
        req.listPersanMissed = listPersanMissed;

        io.Emit("endSong", req.toJson());
    }

    public void TestEndSong()
    {
        string[] values = { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };
        EndSong("song A", 100, values);
    }

    public string FindPath(string songname, string LR)
    {
        MymidiImporter songlist = songlistObj.GetComponent<MymidiImporter>();
        var path = "";
        Debug.Log(LR);
        if (LR == "L")
        {
            foreach (var target in songlist.list_L)
            {
                var files_L = Directory.GetFiles(Application.streamingAssetsPath + "/L");
                foreach (string asset in files_L)
                {
                    if (asset.EndsWith(".wav"))
                    {
                        if (Path.GetFileNameWithoutExtension(asset) == songname+"_L")
                        {
                            path = asset;
                            Debug.Log(path);
                        }
                    }
                }
            }
        }
        if (LR == "R")
        {
            foreach (var target in songlist.list_R)
            {
                var files_R = Directory.GetFiles(Application.streamingAssetsPath + "/R");
                foreach (string asset in files_R)
                {
                    if (asset.EndsWith(".wav"))
                    {
                        if (Path.GetFileNameWithoutExtension(asset) == songname + "_R")
                        {
                            path = asset;
                            Debug.Log(path);
                        }
                    }
                }
            }
        }
        if (LR == "LR")
        {
            foreach (var target in songlist.list_LR)
            {
                var files = Directory.GetFiles(Application.streamingAssetsPath);
                foreach (string asset in files)
                {
                    if (asset.EndsWith(".wav"))
                    {
                        if (Path.GetFileNameWithoutExtension(asset) == songname)
                        {
                            path = asset;
                            Debug.Log(path);
                        }
                    }
                }
            }
        }
        Debug.Log(path);
        return path;
    }
    public SongItem FindSongItem(string songname, string LR)
    {
        MymidiImporter songlist = songlistObj.GetComponent<MymidiImporter>();
        List<SongItem> result = new List<SongItem>();
        if (LR=="L")
        {
            foreach (var item in songlist.list_L)
            {
                if (item.name == songname)
                {
                    result.Add(item);
                }
            }
        }
        if (LR == "R")
        {
            foreach (var item in songlist.list_R)
            {
                if (item.name == songname)
                {
                    result.Add(item);
                }
            }
        }
        if (LR == "LR")
        {
            foreach (var item in songlist.list_LR)
            {
                if (item.name == songname)
                {
                    result.Add(item);
                }
            }
        }

        return result[0];
    }
}
[System.Serializable]
public class login_req
{
    public string username;
    public bool logined;
    public string toJson(){
        return JsonUtility.ToJson(this);
    }
}

[System.Serializable]
public class song_req
{
    public string song;
    public int count;
    public string toJson()
    {
        return JsonUtility.ToJson(this);
    }
}

[System.Serializable]
public class endsong_req
{
    public string song;
    public float score;
    public string username;
    public string[] listPersanMissed;


    public string toJson()
    {
        return JsonUtility.ToJson(this);
    }
}

[System.Serializable]
public class room_req
{
    public string room;
    public bool joined;
    public string toJson()
    {
        return JsonUtility.ToJson(this);
    }
}

[System.Serializable]
public class clients_req
{
    public string room;
    public string username;
    public float score;
    public string[] listPersanMissed;


}

