using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RhythmGameStarter
{
    /// <summary>
    /// A lite data class similar to <see cref="SongItem"/> for data storing with Json
    /// </summary>
    [Serializable]
    public class LiteSongItem
    {
        public string name;
        
        public int bpm;
        public List<SongItem.MidiNote> notes;

        public LiteSongItem(string name, SongItem songItem)
        {
            this.bpm = songItem.bpm;
            this.notes = songItem.notes;
        }

        public static LiteSongItem FromJson(string json)
        {
            return JsonUtility.FromJson(json, typeof(LiteSongItem)) as LiteSongItem;
        }

        public void RecalculateBpmTo(int newBpm)
        {
            //Recalculate the time of each note to the new bpm
            foreach (var note in notes)
            {
                note.time = 60f / newBpm * note.beatIndex;
                note.noteLength = 60f / newBpm * note.beatLengthIndex;
            }

            bpm = newBpm;
        }
    }

    [CreateAssetMenu(fileName = "SongItem", menuName = "Rhythm Game/Song Item", order = 1)]
    public class SongItem : ScriptableObject
    {
        public const int maxTrackNumberForMapping = 14;

        public static NoteName[] noteNameMapping = new NoteName[] { NoteName.C, NoteName.CSharp, NoteName.D, NoteName.DSharp, NoteName.E, NoteName.F ,NoteName.FSharp, NoteName.G, NoteName.GSharp,
        NoteName.A, NoteName.ASharp, NoteName.B,
        NoteName.C3, NoteName.C3Sharp, NoteName.D3, NoteName.D3Sharp, NoteName.E3, NoteName.F3 ,NoteName.F3Sharp, NoteName.G3, NoteName.G3Sharp,
        NoteName.A3, NoteName.A3Sharp, NoteName.B3,
        NoteName.C4, NoteName.C4Sharp, NoteName.D4, NoteName.D4Sharp, NoteName.E4, NoteName.F4 ,NoteName.F4Sharp, NoteName.G4, NoteName.G4Sharp,
        NoteName.A4, NoteName.A4Sharp, NoteName.B4,
        NoteName.C5, NoteName.C5Sharp, NoteName.D5, NoteName.D5Sharp, NoteName.E5, NoteName.F5 ,NoteName.F5Sharp, NoteName.G5, NoteName.G5Sharp,
        NoteName.A5, NoteName.A5Sharp, NoteName.B5,
        NoteName.C6, NoteName.C6Sharp, NoteName.D6, NoteName.D6Sharp, NoteName.E6, NoteName.F6 ,NoteName.F6Sharp, NoteName.G6, NoteName.G6Sharp,
        NoteName.A6, NoteName.A6Sharp, NoteName.B6,
        NoteName.C7};

        public AudioClip clip;

        public float length;

        public string author;

        public int bpm;

        public string hand;

        public MetadataList metadata;

        

        [Serializable]
        public class MetadataList : ReorderableList<Metadata> { }

        [Serializable]
        public class Metadata
        {
            public string id;
            public int intValue;
            public string stringValue;
        }

        public bool TryGetMetadata(string id, out Metadata value)
        {
            var v = metadata.values.Find(x => x.id == id);
            value = v;
            return v != null;
        }

        public bool useCurrentBpmMidiImport;

        [HideInInspector]
        public List<MidiNote> notes = new List<MidiNote>();

        public bool useGenetator;
        public bool runtimeGenerate;

        public SequenceGeneratorBase generator;

        public float onsetSensitivity = 1;
        public float[] onsetData;
        public float onsetMin, onsetMax;

        /// <summary>
        /// Returns a copied version of the notes
        /// </summary>
        /// <returns></returns>
        public List<MidiNote> GetNotes()
        {
            if (useGenetator && runtimeGenerate && generator)
            {
                var temp = generator.OnGenerateSequence(this);
                if (temp != null)
                    return temp;
            }

            return notes.ConvertAll(x => new MidiNote(x));
        }

        [Serializable]
        public class MidiNote
        {
            public NoteName noteName;
            public int noteOctave;
            public float time;
            public float noteLength;
            public float beatIndex;
            public float beatLengthIndex;

            public MidiNote() { }

            public MidiNote(MidiNote other)
            {
                noteName = other.noteName;
                noteOctave = other.noteOctave;
                time = other.time;
                noteLength = other.noteLength;
                beatIndex = other.beatIndex;
                beatLengthIndex = other.beatLengthIndex;
            }

            [NonSerialized]
            public bool created;
        }

        // public TempoChange[] tempoChanges;

        // [Serializable]
        // public class TempoChange
        // {
        //     public float timeStart;
        //     public float timeEnd;
        //     public int bpm;
        // }

        //Rounding our beat to 0.25
        public static float RoundToNearestBeat(float value)
        {
            return (float)Math.Round(value * 4, MidpointRounding.ToEven) / 4;
        }

        // public void ResetNotesState()
        // {
        //     notes.ForEach(x => x.created = false);
        // }

        //Expensive method
        public float[] GenerateOnset()
        {
            var detector = new AudioAnalysis(clip);

            detector.DetectOnsets(onsetSensitivity);
            // detector.NormalizeOnsets(0);
            return detector.GetOnsets();
        }

        public void LoadNotesFrom(LiteSongItem liteSongItem)
        {
            this.notes = liteSongItem.notes;
           
        }

        public enum NoteName
        {
            C = 0,

            CSharp = 1,

            D = 2,

            DSharp = 3,

            E = 4,

            F = 5,
            FSharp = 6,

            G = 7,

            GSharp = 8,

            A = 9,

            ASharp = 10,

            B = 11,//

            B3 = 12,

            C3 = 13,

            C3Sharp = 14,

            D3 = 15,

            D3Sharp = 16,

            E3 = 17,

            F3 = 18,
            F3Sharp = 19,

            G3 = 20,

            G3Sharp = 21,

            A3 = 22,

            A3Sharp = 23,//

            B4 = 24,

            C4 = 25,

            C4Sharp = 26,

            D4 = 27,

            D4Sharp = 28,

            E4 = 29,

            F4 = 30,
            F4Sharp = 31,

            G4 = 32,

            G4Sharp = 33,

            A4 = 34,

            A4Sharp = 35,

            B5 = 36,

            C5 = 37,

            C5Sharp = 38,

            D5 = 39,

            D5Sharp = 40,

            E5 = 41,

            F5 = 42,
            F5Sharp = 43,

            G5 = 44,

            G5Sharp = 45,

            A5 = 46,

            A5Sharp = 47,

            B6 = 48,

            C6 = 49,

            C6Sharp = 50,

            D6 = 51,

            D6Sharp = 52,

            E6 = 53,

            F6 = 54,
            F6Sharp = 55,

            G6 = 56,

            G6Sharp = 57,

            A6 = 58,

            A6Sharp = 59,

            C7 = 60
        }
    }
}