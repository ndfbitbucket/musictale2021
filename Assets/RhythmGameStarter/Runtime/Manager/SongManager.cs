﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using TMPro;
using System.Collections;

namespace RhythmGameStarter
{
    [HelpURL("https://bennykok.gitbook.io/rhythm-game-starter/hierarchy-overview/song-manager")]
    [RequireComponent(typeof(TrackManager))]
    public class SongManager : MonoBehaviour
    {
        public Animator Dino1,Dino11;
        public GameObject UIHitMiss;
        public GameObject UIHitMiss2;
        public bool dino1_eat,dino11_eat;
        public int[] myNumMissed ;
        public int[] myNumHit ;
        public TMP_Text[] list;
        public float[] number ;
        public GameObject[] NoteMissedText;


        public AudioClip[] soundNote;
        public AudioSource soundManager;
        public string[] persan = { };
        [Comment("Responsible for song control, handling song related events.")]
        public AudioSource audioSource;

        [Title("Properties", 0)]
        [Space]
        public bool playOnAwake = true;
        public SongItem defaultSong;
        public float delay;
        public bool looping;

        [Tooltip("Automatically handle play/pause when timescale set to 0, or back to 1")]
        public bool autoTimeScalePause;

        [Title("Display", 0)]
        public bool progressAsPercentage = true;
        public bool inverseProgressFill = false;

        [HideInInspector] public float secPerBeat;
        [HideInInspector] public float songPosition;
        [HideInInspector] public IEnumerable<SongItem.MidiNote> currnetNotes;

        [Title("Events", 0)]
        [CollapsedEvent("Triggered every frame when a song progress")] public FloatEvent onSongProgress;
        [CollapsedEvent("Triggered every frame when a song progress with a float between [0,1] useful for Image fill")] public FloatEvent onSongProgressFill;
        [CollapsedEvent("Triggered every frame the song progress with a string variable of the progress display")] public StringEvent onSongProgressDisplay;
        [CollapsedEvent("Triggered when the song play, after the delay wait time")] public UnityEvent onSongStart;
        [CollapsedEvent("Triggered when the song play, before the delay wait time")] public UnityEvent onSongStartPlay;
        [CollapsedEvent("Triggered when the song finished")] public UnityEvent onSongFinished;
        public GameObject Songname;

        #region RUNTIME_FIELD

        [NonSerialized] public bool songPaused;
        [NonSerialized] public SongItem currentSongItem;
        [NonSerialized] public ComboSystem comboSystem;
        [NonSerialized] public TrackManager trackManager;

        private bool songHasStarted;
        private bool songStartEventInvoked;
        private double dspStartTime;
        private double dspPausedTime;
        private double accumulatedPauseTime;

        #endregion

        private void Awake()
        {
            trackManager = GetComponent<TrackManager>();
            comboSystem = GetComponent<ComboSystem>();

            trackManager.Init(this);


        }

        private void Start()
        {

            if (playOnAwake && defaultSong)
            {
                PlaySong(defaultSong);
            }
        }

        public void PlaySong()
        {
            if (defaultSong)
                PlaySong(defaultSong);
            else
                Debug.LogWarning("Default song is not set!");
        }

        public void PlaySongSelected(SongItem songItem)
        {
            PlaySong(songItem);
        }

        public void SetDefaultSong(SongItem songItem)
        {
            defaultSong = songItem;
        }

        public void PlaySong(SongItem songItem, double specificStartTime = 0)
        {
            currentSongItem = songItem;
            songPaused = false;
            songHasStarted = true;
            accumulatedPauseTime = 0;
            dspPausedTime = 0;
            songPosition = -1;

            if (audioSource) audioSource.clip = songItem.clip;
            
            // songItem.ResetNotesState();
            currnetNotes = songItem.GetNotes();
           // Debug.Log(currnetNotes);
            secPerBeat = 60f / songItem.bpm;

            //Starting the audio play back
            dspStartTime = AudioSettings.dspTime;
            if (audioSource)
            {
                audioSource.PlayScheduled(AudioSettings.dspTime + delay);
                audioSource.time = (float)specificStartTime;
                dspStartTime -= specificStartTime;
            }

            trackManager.SetupForNewSong();

            
            reSetmissNote();
            var textname = Songname.transform.GetChild(0).GetComponent<TMP_Text>();
            textname.text = songItem.name;

            onSongStartPlay.Invoke();
            

           // soundManager.PlayOneShot(soundNote[0]);
           // soundManager.mute=true;

            
        }

        public void PauseSong()
        {
            if (songPaused) return;

            songPaused = true;
            if (audioSource) audioSource.Pause();

            dspPausedTime = AudioSettings.dspTime;
        }

        public void ResumeSong()
        {
            if (!songHasStarted)
            {
                PlaySong();
                return;
            }
            if (!songPaused) return;

            songPaused = false;
            if (audioSource) audioSource.Play();

            accumulatedPauseTime += AudioSettings.dspTime - dspPausedTime;
        }

        public void StopSong(bool dontInvokeEvent = false)
        {
            if (audioSource) audioSource.Stop();
            songHasStarted = false;
            songStartEventInvoked = false;

            if (!dontInvokeEvent)
            {
                onSongFinished.Invoke();
            }
                

            trackManager.ClearAllTracks();
            Songname.SetActive(false);
            
        }

        void Update()
        {
            if (!songStartEventInvoked && songHasStarted && songPosition >= 0)
            {
                songStartEventInvoked = true;
                onSongStart.Invoke();
            }

            // If we need to automatically handle play/pause according to the timescale;
            if (songHasStarted && songStartEventInvoked && autoTimeScalePause)
            {
                if (!songPaused && Time.timeScale == 0)
                {
                    PauseSong();
                }
                else if (songPaused && Time.timeScale == 1)
                {
                    ResumeSong();
                }
            }

            //Sync the tracks position with the audio
            if (!songPaused && songHasStarted)
            {
                songPosition = (float)(AudioSettings.dspTime - dspStartTime - delay - accumulatedPauseTime);

                trackManager.UpdateTrack(songPosition, secPerBeat);

                onSongProgress.Invoke(songPosition);

                //Debug.Log(currentSongItem.length+"    AAAAA");

                if (inverseProgressFill)
                {
                     //onSongProgressFill.Invoke(1 - (songPosition / currentSongItem.clip.length));
                    onSongProgressFill.Invoke(1 - (songPosition / currentSongItem.length));
                }
                else
                {
                   // Debug.Log(currentSongItem.length + "    BBBB");
                    // onSongProgressFill.Invoke(songPosition / currentSongItem.clip.length);
                    onSongProgressFill.Invoke(songPosition / currentSongItem.length);
                    
                }
                if (songPosition >= 0)
                {
                    if (progressAsPercentage)
                        // onSongProgressDisplay.Invoke(Math.Truncate(songPosition / currentSongItem.clip.length * 100) + "%");
                        onSongProgressDisplay.Invoke(Math.Truncate(songPosition / currentSongItem.length * 100) + "%");
                    else
                    {
                        var now = new DateTime((long)songPosition * TimeSpan.TicksPerSecond);
                        onSongProgressDisplay.Invoke(now.ToString("mm:ss"));
                    }
                }
            }

            //if (songHasStarted && currentSongItem.clip && songPosition >= currentSongItem.clip.length)
            if (songHasStarted  && songPosition >= currentSongItem.length)
            {
                songHasStarted = false;
                songStartEventInvoked = false;
                onSongFinished.Invoke();

                trackManager.ClearAllTracks();

                Debug.Log("Stop-----------");
               // updatTextPersan();

                //If its looping, we replay the current song
                if (looping)
                    PlaySong(currentSongItem);
            }
        }
        
        public void HitNote(string Note)
        {
            
            //  Dino = GetComponent<Animator>();
            if (Note=="8"||Note == "9"||Note == "10"||Note == "11"||Note == "12" || Note == "13"|| Note == "14")
            {
                
                Debug.Log("AnimDino11");
                // Dino1.SetInteger("animation", 5);
                if (Note == "8" || Note == "9" || Note == "10" || Note == "11" )
                {
                    Dino11.SetTrigger("Iseat");
                }
                if( Note == "12" || Note == "13" || Note == "14")
                {
                    Dino11.SetTrigger("Iseat");
                }
                dino11_eat = true;
                StartCoroutine(RunBabyGrayMainAnimation("Dino11"));

                TextMeshProUGUI text = UIHitMiss.GetComponentInChildren<TextMeshProUGUI>();
                text.SetText("Perfect");
                text.color = Color.gray;
                UIHitMiss.SetActive(true);
                StartCoroutine(WaitUI(UIHitMiss));
            }
            if (Note == "1" || Note == "2" || Note == "3" || Note == "4" || Note == "5" || Note == "6"|| Note == "7")
            {
             
                Debug.Log("AnimDino1" );
                // Dino11.SetInteger("animation", 5);
                if (Note == "1" || Note == "2" || Note == "3" || Note == "4" )
                {
                    Dino1.SetTrigger("Iseat");
                }
                if (Note == "5" || Note == "6" || Note == "7")
                {
                    Dino1.SetTrigger("Iseat");
                }
                dino1_eat = true;
               
                StartCoroutine(RunBabyGrayMainAnimation("Dino1"));

                TextMeshProUGUI text = UIHitMiss2.GetComponentInChildren<TextMeshProUGUI>();
                text.SetText("Perfect");
                text.color = Color.gray;
                UIHitMiss2.SetActive(true);
                StartCoroutine(WaitUI(UIHitMiss2));
            }

            int number = int.Parse(Note) - 1;
            myNumHit[number]++;
            Debug.Log("eat " + Note);
           


        }
        IEnumerator WaitUI(GameObject o)
        {
            yield return new WaitForSeconds(0.8f);
            o.SetActive(false);
        }
        IEnumerator RunBabyGrayMainAnimation(string name)
        {
            yield return new WaitForSeconds(0.3f);
            if (name == "Dino1")
            {
                dino1_eat = false;
            }
            if (name == "Dino11")
            {
                dino11_eat = false;
            }

        }
        public void missNote(string Note)
        {
            int numbermiss = int.Parse(Note) - 1;
            myNumMissed[numbermiss]++;
            Debug.Log("MissdNote " + Note);
            TextMeshProUGUI text = UIHitMiss.GetComponentInChildren<TextMeshProUGUI>();
            text.SetText("Miss");
            text.color = Color.red;
            UIHitMiss.SetActive(true);
            StartCoroutine(WaitUI(UIHitMiss));
            TextMeshProUGUI text2 = UIHitMiss2.GetComponentInChildren<TextMeshProUGUI>();
            text2.SetText("Miss");
            text2.color = Color.red;
            UIHitMiss2.SetActive(true);
            StartCoroutine(WaitUI(UIHitMiss2));
        }
        public void reSetmissNote()
        {
            Debug.Log("ReSet");

            for (int i = 0; i < NoteMissedText.Length; i++)
            {
                // Debug.Log("Set");
                var textmissednote0 = NoteMissedText[i].GetComponent<TMP_Text>();
                myNumMissed[i] = 0;
                myNumHit[i] = 0;
                textmissednote0.text = 0.ToString();
            }
        }
        public void updatTextPersan()
        {
           
            for (int i=0;i< list.Length; i++)
            {
                 list[i] = NoteMissedText[i].GetComponent<TMP_Text>();
                number[i] = (float)myNumHit[i] / ((float)myNumMissed[i] + (float)myNumHit[i]);
               // Debug.Log(number[i]);
                if (float.IsNaN(number[i]))
                {
                    list[i].text = 0.ToString();
                    persan[i]= 0.ToString();
                }
                else
                {
                    list[i].text = Mathf.Round(number[i] * 100).ToString() + "%";
                    persan[i] = Mathf.Round(number[i] * 100).ToString() + "%";
                    Debug.Log("Texttt");
                }
                
            }
        }



        }
}