﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RhythmGameStarter
{
    public class ColorfulNotesHandler : MonoBehaviour
    {
        [Comment("Simple script to handle note init callback from TrackManager and assign random color to the notes. Also takes care of most of the custom logic for this demo.")]
        public List<ColorEntry> randomColors;

        private SongManager songManager;

        private void Awake() {
            songManager = GameObject.FindObjectOfType<SongManager>();
        }

        private void Start()
        {
            randomColors.ForEach(x =>
            {
                x.label = x.uiDisplay.GetComponentInChildren<TMPro.TextMeshProUGUI>();
                x.label.text = "-";

                x.uiDisplay.GetComponentInChildren<Image>().color = x.color;
            });

            songManager.onSongStart.AddListener(() =>
            {
                randomColors.ForEach(x =>
                {
                    x.label.text = "-";
                    x.count = 0;
                });
            });
        }

        //For receiving call back from the TrackManager's (onNoteInit) event, when a note is being init
        public void OnNoteInit(Note note)
        {
            //Debug.Log(note.noteType);
            var track = int.Parse(note.parentTrack.name) - 1;
            var whattrack = int.Parse(note.parentTrack.name);
            //Debug.Log(note.parentTrack.name);
            //var selectedColor = randomColors[Random.Range(0, randomColors.Count)];
            var selectedColor = randomColors[track];
            //Loop through all the notes, then assign a random color to them
            foreach (var renderer in note.GetComponentsInChildren<SpriteRenderer>())
            {
                //if (renderer.name != "Swipe Indicator")
                //{
                //    renderer.color = selectedColor.color;
                //    renderer.sprite = selectedColor.sprite[note.noteType];
                //}
                if (renderer.name == "Square")
                {
                    if (whattrack == 1)
                        renderer.sprite = selectedColor.sprite[0];
                    if (whattrack == 2)
                        renderer.sprite = selectedColor.sprite[1];
                    if (whattrack == 3)
                        renderer.sprite = selectedColor.sprite[2];
                    if (whattrack == 4)
                        renderer.sprite = selectedColor.sprite[3];
                    if (whattrack == 5)
                        renderer.sprite = selectedColor.sprite[3];
                    if (whattrack == 6)
                        renderer.sprite = selectedColor.sprite[2];
                    if (whattrack == 7)
                        renderer.sprite = selectedColor.sprite[1];
                    if (whattrack == 8)
                        renderer.sprite = selectedColor.sprite[0];
                    if (whattrack == 9)
                        renderer.sprite = selectedColor.sprite[0];
                    if (whattrack == 10)
                        renderer.sprite = selectedColor.sprite[1];
                    if (whattrack == 11)
                        renderer.sprite = selectedColor.sprite[2];
                    if (whattrack == 12)
                        renderer.sprite = selectedColor.sprite[3];
                }
                else if (renderer.name == "Square Long")
                {
                    if (whattrack == 1)
                        renderer.sprite = selectedColor.sprite[4];
                    if (whattrack == 2)
                        renderer.sprite = selectedColor.sprite[5];
                    if (whattrack == 3)
                        renderer.sprite = selectedColor.sprite[6];
                    if (whattrack == 4)
                        renderer.sprite = selectedColor.sprite[7];
                    if (whattrack == 5)
                        renderer.sprite = selectedColor.sprite[7];
                    if (whattrack == 6)
                        renderer.sprite = selectedColor.sprite[6];
                    if (whattrack == 7)
                        renderer.sprite = selectedColor.sprite[5];
                    if (whattrack == 8)
                        renderer.sprite = selectedColor.sprite[4];
                    if (whattrack == 9)
                        renderer.sprite = selectedColor.sprite[4];
                    if (whattrack == 10)
                        renderer.sprite = selectedColor.sprite[5];
                    if (whattrack == 11)
                        renderer.sprite = selectedColor.sprite[6];
                    if (whattrack == 12)
                        renderer.sprite = selectedColor.sprite[7];
                }

            }

            //We appends the color name to the the note object, so we can recognize it back later on
            note.name = selectedColor.name;
        }

        //For receiving call back from the TrackManager's (onNoteTriggered) event, when a note is being hit
        public void OnNoteTriggered(Note note)
        {
            //Loop through all the notes to find a match one, and we increase the corresponding count, and also update the UI 
            foreach (var color in randomColors)
            {
                if (note.name == color.name)
                {
                    color.count++;
                    color.label.text = color.count.ToString();
                    break;
                }
            }
        }
    }

    [System.Serializable]
    public class ColorEntry
    {
        public string name;
        public Sprite[] sprite;
        public Color color;
        public GameObject uiDisplay;

        
        [System.NonSerialized] public TMPro.TextMeshProUGUI label;
        [System.NonSerialized] public float count;
    }
}
