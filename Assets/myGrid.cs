using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myGrid : MonoBehaviour
{
    public float space;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    [ContextMenu("Update")]
public void updateGrid()
    {
        var startpos = Vector3.zero;
        var index = 0;
        foreach (Transform item in transform)
        {
            if (item.gameObject.activeSelf == false)
                continue;

            item.localPosition = startpos + new Vector3(index*space, 0, 0);
            index++;
        }
    }
}
