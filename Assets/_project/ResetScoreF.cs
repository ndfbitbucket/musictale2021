using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using RhythmGameStarter;

public class ResetScoreF : MonoBehaviour
{
    public TextMeshProUGUI TextScore;
    public TextMeshProUGUI TextPercentage;
    public Animator songpercentage;
    public GameObject Pause;
    public GameObject Resume;
    public Button pauseBT;
    public GameObject StartMenu;
    public SongManager songManager;
    public GameObject Result;

    // Start is called before the first frame update
    public void ResetClick()
    {
        TextScore.text = "0000";
        TextPercentage.text = "0%";
        songpercentage.SetTrigger("Reset");
        Pause.SetActive(true);
        Resume.SetActive(false);
        pauseBT.interactable = false;
        StartMenu.SetActive(true);
        songManager.StopSong(true);
        Result.SetActive(false);
    }
}
