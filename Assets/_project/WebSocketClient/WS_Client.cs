using System.Collections;
using System.Collections.Generic;
using WebSocketSharp;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class WS_Client : MonoBehaviour
{
    public User _currentUser;
    WebSocket ws;
    public TMP_InputField _inputField;
    public GameObject _listMusicPage;
    public GameObject _gamePlay;

    public List<Music> _listMusic = new List<Music>();
    // Start is called before the first frame update
    void Start()
    {
        ws = new WebSocket("ws://localhost:8080");
        ws.OnMessage += (sender, e) =>
        {
            Debug.Log("Message received form " + ((WebSocket)sender).Url + ", Data :" + e.Data);
        };
        ws.Connect();
        
    }
    private void OnApplicationQuit()
    {
        //ws.OnClose();
    }
    // Update is called once per frame
    void Update()
    {
        if (ws == null)
        {
            return;
        }
            
        if (Input.GetKeyDown(KeyCode.Space))
        {
            print("Close");
            ws.Close();
        }
    }

    public void SelectMode(int index)
    {
        User user = new User();
        if (index == 0)
        {
            user.mode = "teacher";
            print("Mode Teacher");
        }
        else if(index == 1)
        {
            user.mode = "student";
            print("Mode Student");
        }

        //ws.Send(JsonUtility.ToJson(user));
        _currentUser = user;
        //print(JsonUtility.ToJson(user));
    }

    public void ConfirmName()
    {
        if (_inputField.text != "")
        {
            _currentUser.name = _inputField.text;
            ws.Send(JsonUtility.ToJson(_currentUser));
        }

        if (_currentUser.mode == "teacher")
        {
            _listMusicPage.gameObject.SetActive(true);
        }
        else if (_currentUser.mode == "student")
        {
            _gamePlay.gameObject.SetActive(true);
        }
    }

    public void SelectMusic(string music)
    {
        if (_currentUser.mode == "teacher")
        {
            _currentUser.music = music;
            
        }
    }
}
[SerializeField]
public class User
{
    public string mode;
    public string name;
    public string music;
}

[SerializeField]
public class Music
{
    public string name;
}
