using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using System.IO;
using TMPro;
public class MyVDOList : MonoBehaviour
{
    public List<string> list_VDO;
    public GameObject Level_List;
    public Transform popupVODlist;
    public GameObject PrefabBtnPlay;
    public VDOManager manager;
    public GameObject UI;
    // Start is called before the first frame update
    void Start()
    {

    }
    [ContextMenu("loadPathVDO")]
    public void loadPathVDO()
    {
        clearRefreshBtnPLay();

        list_VDO = new List<string>();
        var files = Directory.GetFiles(Application.streamingAssetsPath);
        foreach (string asset_Level in files)
        {
            Debug.Log(asset_Level);
            string Levelname;
            Levelname = Path.GetFileNameWithoutExtension(asset_Level);
            var file_Song = Directory.GetFiles(Application.streamingAssetsPath + "/" + Levelname);
            foreach (string asset_S in file_Song)
            {
                string songName;
                songName = Path.GetFileNameWithoutExtension(asset_S);
                var files_VDO = Directory.GetFiles(Application.streamingAssetsPath + "/" + Levelname + "/" + songName + "/VDO");
                foreach (string asset_VDO in files_VDO)
                {
                    if (asset_VDO.EndsWith(".mp4"))
                    {
                        list_VDO.Add(asset_VDO);
                    }
                }

            }
        }
    }
    [ContextMenu("RefreshBtn")]
    public void RefreshBtnPLay()
    {
        int i = 0;
        var files = Directory.GetFiles(Application.streamingAssetsPath);

        foreach (string asset_Level in files)
        {

            Debug.Log(asset_Level);

            string Levelname;
            Levelname = Path.GetFileNameWithoutExtension(asset_Level);
            Debug.Log(Levelname);

            var selectLevel = Level_List.GetComponent<LevelManager>();

            Debug.Log(selectLevel.NowLevel);
            if (selectLevel.NowLevel == Levelname)
            {
                foreach (var item in list_VDO)
                {
                    i++;
                    var files_song = Directory.GetFiles(Application.streamingAssetsPath + "/" + Levelname);
                    foreach (string asset_S in files_song)
                    {
                        string songName;
                        songName = Path.GetFileNameWithoutExtension(asset_S);

                        var files_VDO = Directory.GetFiles(Application.streamingAssetsPath + "/" + Levelname + "/" + songName + "/VDO");

                        foreach (string asset_VDO in files_VDO)
                        {
                            string VDOname;
                            VDOname = Path.GetFileNameWithoutExtension(asset_VDO);
                            Debug.Log(asset_VDO);
                            Debug.Log(item);
                            if (asset_VDO == item)
                            {
                                var btn = Instantiate(PrefabBtnPlay, popupVODlist);
                                var script = btn.GetComponent<BtnPlayVDO>();
                                script.VDOnumber = (i - 1);
                                btn.name = VDOname;
                                script.T1.SetText(VDOname);
                            }
                        }

                    }
                }
            }
        }
    }
    [ContextMenu("playVDO")]
    public void PlayVDO(int VDONumber)
    {
       
        Debug.Log(list_VDO[VDONumber]);
        var path = list_VDO[VDONumber];
        Debug.Log("Path");
        Debug.Log(path);
        manager.GetComponent<VDOManager>().PlayURL(path);

       // list_VDO[game.GetComponent<GameHelper>().VDONumber];
    }

    public void BtnPlay(int VDONumber)
    {
        PlayVDO(VDONumber);

        manager.gameObject.SetActive(true);
        UI.SetActive(false);
        
    }
    public void clearRefreshBtnPLay()
    {

        foreach (Transform child in popupVODlist)
        {
            GameObject.Destroy(child.gameObject);
        }

    }
}
