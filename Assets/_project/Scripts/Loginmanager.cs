using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using RhythmGameStarter;
using UnitySocketIO;
using UnityEngine.SceneManagement;
public class Loginmanager : MonoBehaviour
{
    public Button yourButton;
    public TMP_InputField usernameInput;
    public GameObject starterUI;
    public GameObject StudentUI;
    public GameObject BG;
    public GameObject Float;
    public GameObject ResultS;
    public GameObject ResultT;
    public GameObject[] Effects;

    public MymidiImporter songlistObj;
    public Client socketClients;

    // Start is called before the first frame update

        void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);

       // var ioB = FindObjectOfType<SocketIOController>();
       // Client.Instance.io = ioB;
        var songlistObjB = FindObjectOfType<MymidiImporter>();
        Client.Instance.songlistObj = songlistObjB;
        //Client socketClient = socketClients.GetComponent<Client>();
        if (Client.Instance.stat == "teacher")
        {
            

        }
        if (Client.Instance.stat == "Student")
        {
            SetStudent();
           
            
        }

    }
    void TaskOnClick()
    {
        MymidiImporter songlist = songlistObj.GetComponent<MymidiImporter>();
       // Client socketClient = socketClients.GetComponent<Client>();
        Debug.Log("You have clicked the buttonLogin!");
        Debug.Log("UserName  " + usernameInput.text);
        //Debug.Log("PassWord  "+passwordInput.text);
        if (usernameInput.text != "")
        {

            if (usernameInput.text == "Teacher")
            {
                Debug.Log("Can Next UI teacher");
               // socketClients.stat = "teacher";
                starterUI.SetActive(false);
                Debug.Log("Client Log Name :"+ usernameInput.text);
                Client.Instance.Login(usernameInput.text, joined =>
                {
                    if (joined)
                    {
                        //Loadscene
                        Client.Instance.stat = "teacher";
                        Client.Instance.Joinroom("A", logined =>
                        {
                            if (logined)
                            {
                                //Login Done
                                Debug.Log("Join----------------T");
                                SceneManager.LoadScene("DEMO 3");
                            }
                        });
                       


                    }

                });




            }
            else
            {
                Debug.Log("Can Next UI Student");
               // socketClients.stat = "Student";
                starterUI.SetActive(false);
                Debug.Log("Client Log Name :" + usernameInput.text);
                Client.Instance.Login(usernameInput.text, joined =>
                {
                    if (joined)
                    {

                        Client.Instance.stat = "Student";
                        Client.Instance.Joinroom("A", logined =>
                        {
                            if (logined)
                            {
                                //Login Done
                                Debug.Log("Join----------------S");
                                SceneManager.LoadScene("DEMO 4");
                            }
                        });
                        

                    }
                });

            }

        }
        else
        {
            Debug.Log("Can't Next UI");
        }


    }
    public void SetCloseManager()
    {
        MymidiImporter songlist = songlistObj.GetComponent<MymidiImporter>();
        if (socketClients.stat =="teacher")
        {
          
            songlist.RefreshUI();
            BG.SetActive(true);
            Float.SetActive(false);
            ResultT.SetActive(false);
            StudentUI.SetActive(false);

        }
        if (socketClients.stat == "Student")
        {
            BG.SetActive(true);
            Float.SetActive(true);
            ResultS.SetActive(false);
            StudentUI.SetActive(true);
        }

    }
    public void SetCloseEff()
    {
        foreach (var e in Effects)
        {
            e.SetActive(false);
        }
    }
    public void SetTeacher()
    {
        MymidiImporter songlist = songlistObj.GetComponent<MymidiImporter>();

        //songlist.loadfile();
        songlist.loadfiletest();
        songlist.RefreshUI();

    }
    public void SetStudent()
    {
        MymidiImporter songlist = songlistObj.GetComponent<MymidiImporter>();
        foreach (var e in Effects)
        {
            e.SetActive(true);
        }
        //songlist.loadfile();
        songlist.loadfiletest();
        //songlist.SetOnPlay();
        StudentUI.SetActive(true);
        songlistObj.clearRefreshUI();
        BG.SetActive(true);

    }
    public void Logout()
    {
        SceneManager.LoadScene("Menu");
        Client.Instance.stat = "";
    }
}

