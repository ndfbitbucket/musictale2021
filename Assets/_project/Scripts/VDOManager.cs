using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class VDOManager : MonoBehaviour,IDragHandler,IPointerDownHandler
{
    [SerializeField]
    public VideoPlayer video;

    public GameObject UI;
    public Image progress;

  
    void Awake()
    {
        video.loopPointReached += OnMovieFinished; // loopPointReached is the event for the end of the video
        video.Play();
        //progress = GetComponent<Image>();
    }
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        if (video.frameCount > 0)
        {
            progress.fillAmount = (float)video.frame / (float)video.frameCount;
        }
            
    }
    public void OnDrag (PointerEventData eventData)
    {
        TrySkip(eventData);
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        TrySkip(eventData);
    }
    private void TrySkip(PointerEventData eventData)
    {
        Vector2 localPoint;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(progress.rectTransform, eventData.position,null,out localPoint))
        {
            float pct = Mathf.InverseLerp(progress.rectTransform.rect.xMin, progress.rectTransform.rect.xMax,localPoint.x);
                SkipToPercent(pct);
        }
    }
    public void SkipToPercent(float pct)
    {
        var frame = video.frameCount * pct;
        video.frame = (long)frame;
    }
    void OnMovieFinished(VideoPlayer player)
    {
        Debug.Log("Event for movie end called");
        player.Stop();
        UI.SetActive(true);
        
        gameObject.SetActive(false);
    }
    public void OnButtonPause()
    {
        video.Pause();
    }

    public void OnButtonPlay()
    {
        video.Play();
    }
    public void PlayURL(string path)
    {
        video.url=path;
    }
}
