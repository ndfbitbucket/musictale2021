using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
public class DetailStudent : MonoBehaviour
{
    public string username;
    public TextMeshProUGUI label;
    public void UpdateDetail()
    {
        
        var result = GameObject.FindObjectOfType<ResultManager>();
        var data = result.list.FirstOrDefault(x => x.username == username);
        result.OpenDetail(data);
        
        var head = GetComponent<GameObject>();
        head = GameObject.FindWithTag("HeadText");
        head.SetActive(false);
    }
    


    }
