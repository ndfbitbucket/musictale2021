using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GamePLaySettingUI : MonoBehaviour
{
    public bool select;
    public Image iconHand;
 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void click()
    {
        var list = transform.parent.GetComponentsInChildren<GamePLaySettingUI>();
        foreach (var item in list)
        {
                item.select = false;
            item.iconHand.transform.localScale = new Vector3(1, 1, 1);

        }


        Select();
    }
    public void Select()
    {
        iconHand.transform.localScale = new Vector3(1.4f, 1.4f, 1.4f);
    }
}
