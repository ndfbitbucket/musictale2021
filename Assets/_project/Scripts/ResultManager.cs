using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnitySocketIO;
using RhythmGameStarter;
using System.Linq;
public class ResultManager : MonoBehaviour
{
    public Client socketClients;
    public SongManager songManager;
    public string[] listPersanMissed = { };

    public GameObject ResultT;
    public GameObject ResultS;

    public GameObject Dino;

    public List<endsong_req> list;
    public GameObject prefab;
    public Transform root;

    public GameObject PanelAccuracy;
    public TextMeshProUGUI[] NoteMissedText;

    // Start is called before the first frame update
    void Start()
    {

    }
    public void OpenDetail(endsong_req data)
    {
        PanelAccuracy.SetActive(true);
        for (int i = 0; i < NoteMissedText.Length; i++)
        {
            NoteMissedText[i].SetText(data.listPersanMissed[i]);
        }
    }
    [ContextMenu("Test")]
    public void SendTextResultStudent()
    {
        SongManager Manager = songManager.GetComponent<SongManager>();
       // Client socketClient = socketClients.GetComponent<Client>();

        listPersanMissed = Manager.persan;
        Debug.Log(this.listPersanMissed);

        if (Client.Instance.stat == "Student")
        {
            Client.Instance.EndSong(Manager.defaultSong.name, 100, listPersanMissed);
        }
       
    }
    public void ShowResult()
    {
       // Client socketClient = socketClients.GetComponent<Client>();
        if (Client.Instance.stat == "teacher")
        {
            ResultT.SetActive(true);
            Dino.SetActive(false);
            Client.Instance.io.Emit("CheckCount", Client.Instance.EMPTYJSON, (json) =>
            {
                var a = JsonUtility.FromJson<checkcount>(json);
                foreach (var item in a.users)
                {
                    if (item.username != "Teacher")
                    {
                        var go = Instantiate<GameObject>(prefab);
                        go.transform.SetParent(root, false);
                        var script = go.GetComponent<DetailStudent>();

                        script.label.SetText(item.username);
                        script.username = item.username;

                        
                    } 
                }

            });
        }
        if (Client.Instance.stat == "Student")
        {
            ResultS.SetActive(true);
            Dino.SetActive(false);


        }
    }
    public void CleaList()
    { 
      list= new List<endsong_req>();
    }
        public void ClearStudentResult()
    {
        foreach (Transform child in root)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

        [System.Serializable]
    public class checkcount
    {
        public List<user> users;
    }
    [System.Serializable]
    public class user
    {
        public string username;
    }
}
