using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class math
{
    public static float Lerp(float a, float b, float t)
    {
        return a * (1 - t) + b * t;
    }
    public static Vector3 Lerp(Vector3 a, Vector3 b, float t)
    {
        return new Vector3(Lerp(a.x, b.x, t), Lerp(a.y, b.y, t), Lerp(a.z, b.z, t));
    }
}
