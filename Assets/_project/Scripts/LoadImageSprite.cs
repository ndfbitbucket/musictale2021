using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Sprites;
public class LoadImageSprite : MonoBehaviour
{
    public SpriteRenderer m_SpriteRenderer;
    public string NameImg;
    public string LayerName;
    public string songNames;
   // public string Level;
    private Sprite mySprite;
    // Start is called before the first frame update
    void Start()
    {



    }
    public IEnumerator LoadImg(string Name,string Level)
    {
        Debug.Log("Setname ---" + Name);

        var files = Directory.GetFiles(Application.streamingAssetsPath);
        var path = Application.streamingAssetsPath + "/"+ Level + "/" + Name + "/ImageSprite/" + LayerName +"/"+ NameImg + ".png";
        Debug.Log(path);
        Debug.Log(NameImg);
        if (File.Exists(path))
        {
            yield return StartCoroutine(LoadSprite(path));
        }
        //foreach (string asset in files)
        //{
        //    string songName;
        //    songName = Path.GetFileNameWithoutExtension(asset);
        //    Debug.Log(songName);
        //    Debug.Log(Name);
        //    if (Name == songName)
        //    {
        //        //var files_Image = Directory.GetFiles(Application.streamingAssetsPath + "/" + songName + "/ImageSprite/" + LayerName);

        //        //foreach (string asset_I in files_Image)
        //        //{
        //        //    if (asset_I.EndsWith(".png"))
        //        //    {
        //        //        string nameImg;
        //        //        nameImg = Path.GetFileNameWithoutExtension(asset_I);
        //        //        Debug.Log(nameImg);
        //        //        Debug.Log(NameImg);
        //        //        if (nameImg == NameImg)
        //        //        {
        //        //            Debug.Log(asset_I);
        //        //            StartCoroutine(LoadSprite(asset_I));

        //        //        }
        //        //    }
        //        //}
        //    }

        //}
    }
    public IEnumerator LoadSprite(string absoluteImagePath)
    {
        Debug.Log("Load");
        Debug.Log(absoluteImagePath);
        string url = absoluteImagePath;
        WWW www = new WWW(url);
        yield return www;
        Texture2D newTankSkin = www.texture;
        mySprite = Sprite.Create(newTankSkin, new Rect(0.0f, 0.0f, newTankSkin.width, newTankSkin.height), new Vector2(0.5f, 0.5f));
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_SpriteRenderer.sprite = mySprite;
    }
    public void SetSongName(string Name)
    {
        songNames = Name;
        Debug.Log("Setname ---" + songNames);
    }
}
