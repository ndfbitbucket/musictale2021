using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class CountdownController : MonoBehaviour
{
    public int countdownTime;
    public TextMeshProUGUI countdownDisplay;
    private void Start()
    {
        //StartCoroutine(CountdownToStart());  
    }
    IEnumerator CountdownToStart()
    {
        while (countdownTime > 0)
        {
            countdownDisplay.text= countdownTime.ToString();
            yield return new WaitForSeconds(1f);
            countdownTime--;


        }
        countdownDisplay.text = "GO";
        yield return new WaitForSeconds(1f);
        countdownDisplay.gameObject.SetActive(false);
    }
    public void StartCountDown()
    {
        
        countdownTime = 3;
        countdownDisplay.gameObject.SetActive(true);
        StartCoroutine(CountdownToStart());
        
    }
}
