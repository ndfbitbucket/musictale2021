﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Melanchall.DryWetMidi.Core;
using Melanchall.DryWetMidi.Interaction;
using UnityEngine;
using System;
using TMPro;
using UnitySocketIO;
namespace RhythmGameStarter
{
    public class MymidiImporter : MonoBehaviour
    {
        public List<SongItem> list_L;
        public List<SongItem> list_R;
        public List<SongItem> list_LR;

        
        public Transform container;

        public RealNoteControler realnote;
        public RealNoteControler fruitnote;

        public GameObject PrefabSelectSong;
        

        public GameObject Level_List;
        //GameObjectSet
       
        public GameObject LoadingUI;
        public GameObject BGUI;
        public GameObject Float;
        public GameObject Dino;
        public GameObject Selection;
        public GameObject[] Effects;
        public GameObject[] Line;
        public GameObject Backgourd;
        public GameObject Songname;
        public GameObject Triger;

        public GameObject Tracks;
        public GameObject StudentUI;
        public GameObject Stop;
        public GameObject CountDown;

        public Client socketClients;

        public int typehand=0;
        protected int index=0;
        protected string Path_L = "";
        protected string Path_R = "";
        protected string Path_LR = "";

        public void Start()
        {

        }
        [ContextMenu("Testsong")]
        public void loadfiletest()
        {

            var files = Directory.GetFiles(Application.streamingAssetsPath);
            list_L = new List<SongItem>();
            list_R = new List<SongItem>();
            list_LR = new List<SongItem>();
           

            foreach (string asset_Level in files)
            {

                Debug.Log(asset_Level);

                string Levelname;
                Levelname = Path.GetFileNameWithoutExtension(asset_Level);
                var file_Song = Directory.GetFiles(Application.streamingAssetsPath + "/" + Levelname);
                foreach (string asset_S in file_Song)
                {
                    string songName;
                    songName = Path.GetFileNameWithoutExtension(asset_S);
                    var files_L = Directory.GetFiles(Application.streamingAssetsPath + "/" + Levelname + "/" + songName + "/L");
                    var files_R = Directory.GetFiles(Application.streamingAssetsPath + "/" + Levelname + "/" + songName + "/R");
                    var files_LR = Directory.GetFiles(Application.streamingAssetsPath + "/" + Levelname + "/" + songName + "/Both");
                    foreach (string asset_L in files_L)
                    {
                        if (asset_L.EndsWith(".mid"))
                        {
                            Debug.Log(asset_L);
                            var detectedBpm = -1;
                            var bpmString = "";

                            var rawMidi = MidiFile.Read(asset_L);

                            SongItem songItem =
                                ScriptableObject.CreateInstance<SongItem>();

                            if (songItem.bpm == 0 && detectedBpm != -1)
                                songItem.bpm = detectedBpm;

                            string songName_L;

                            songName_L = Path.GetFileNameWithoutExtension(asset_L);
                            Debug.Log(songName_L);
                            string[] splitArray_L = songName_L.Split(char.Parse("_"));
                            for (int i = 0; i < splitArray_L.Length; i++)
                            {
                                songName_L = splitArray_L[0];
                                Debug.Log(songName_L);
                            }


                            foreach (string asset_R in files_R)
                            {
                                if (asset_R.EndsWith(".mid"))
                                {
                                    var detectedBpm_R = -1;
                                    var bpmString_R = "";

                                    var rawMidi_R = MidiFile.Read(asset_R);

                                    SongItem songItem_R =
                                        ScriptableObject.CreateInstance<SongItem>();

                                    if (songItem_R.bpm == 0 && detectedBpm_R != -1)
                                        songItem_R.bpm = detectedBpm_R;

                                    string songName_R;
                                    songName_R = Path.GetFileNameWithoutExtension(asset_R);
                                    string[] splitArray_R = songName_R.Split(char.Parse("_"));
                                    for (int i = 0; i < splitArray_R.Length; i++)
                                    {
                                        songName_R = splitArray_R[0];
                                        Debug.Log(songName_R);
                                    }
                                    if (songName_L == songName_R)
                                    {
                                        songItem_R.name = songName_R;
                                        songItem_R.hand = "Right";

                                        var read = transform.GetComponent<ReadjsonSongInfo>();
                                        //test
                                        foreach (var item in read.items)
                                        {
                                            if (item.songname == songItem_R.name)
                                            {
                                                songItem_R.length = item.length;
                                            }
                                        }


                                        updateBmp(rawMidi_R, songItem_R);
                                        list_R.Add(songItem_R);

                                        songItem.name = songName_L;
                                        songItem.hand = "Leaf";
                                        //test
                                        Debug.Log(songItem.name);
                                        foreach (var item in read.items)
                                        {
                                            if (item.songname == songItem.name)
                                            {
                                                songItem.length = item.length;
                                            }
                                        }

                                        updateBmp(rawMidi, songItem);
                                        list_L.Add(songItem);

                                    }
                                }
                            }
                        }
                    } 
                    //LR
                    foreach (string asset_LR in files_LR)
                    {
                        if (asset_LR.EndsWith(".mid"))
                        {
                            var detectedBpm_LR = -1;
                            var bpmString_LR = "";

                            var rawMidi_LR = MidiFile.Read(asset_LR);

                            SongItem songItem_LR =
                                ScriptableObject.CreateInstance<SongItem>();

                            if (songItem_LR.bpm == 0 && detectedBpm_LR != -1)
                                songItem_LR.bpm = detectedBpm_LR;

                            string songName_LR;
                            songName_LR = Path.GetFileNameWithoutExtension(asset_LR);

                            songItem_LR.name = songName_LR;
                            songItem_LR.hand = "LeafRight";
                            Debug.Log(songItem_LR.name + "      Name");
                            var read = transform.GetComponent<ReadjsonSongInfo>();
                            foreach (var item in read.items)
                            {

                                if (item.songname == songName_LR)
                                {

                                    songItem_LR.length = item.length;
                                }
                            }
                            //songItem_LR.length = 13f;
                            updateBmp(rawMidi_LR, songItem_LR);
                            list_LR.Add(songItem_LR);


                        }
                    }


                }

            }




        }
            [ContextMenu("Test")]
        public void loadfile()
        {
            var files = Directory.GetFiles(Application.streamingAssetsPath);
            var files_L = Directory.GetFiles(Application.streamingAssetsPath + "/L");
            var files_R = Directory.GetFiles(Application.streamingAssetsPath + "/R");
            list_L = new List<SongItem>();
            list_R = new List<SongItem>();
            list_LR = new List<SongItem>();
            foreach (string asset in files_L)
            {
                if (asset.EndsWith(".mid"))
                {
                    var detectedBpm = -1;
                    var bpmString = "";

                    var rawMidi = MidiFile.Read(asset);

                    SongItem songItem =
                        ScriptableObject.CreateInstance<SongItem>();

                    if (songItem.bpm == 0 && detectedBpm != -1)
                        songItem.bpm = detectedBpm;

                    string songName;

                    songName = Path.GetFileNameWithoutExtension(asset);
                    Debug.Log(songName);
                    string[] splitArray_L = songName.Split(char.Parse("_"));
                    for (int i = 0; i < splitArray_L.Length; i++)
                    {
                        songName = splitArray_L[0];
                        Debug.Log(songName);
                    }


                    foreach (string asset_R in files_R)
                    {
                        if (asset_R.EndsWith(".mid"))
                        {
                            var detectedBpm_R = -1;
                            var bpmString_R = "";

                            var rawMidi_R = MidiFile.Read(asset_R);

                            SongItem songItem_R =
                                ScriptableObject.CreateInstance<SongItem>();

                            if (songItem_R.bpm == 0 && detectedBpm_R != -1)
                                songItem_R.bpm = detectedBpm_R;

                            string songName_R;
                            songName_R = Path.GetFileNameWithoutExtension(asset_R);
                            string[] splitArray_R = songName_R.Split(char.Parse("_"));
                            for (int i = 0; i < splitArray_R.Length; i++)
                            {
                                songName_R = splitArray_R[0];
                                Debug.Log(songName_R);
                            }
                            if (songName== songName_R)
                            {
                                songItem_R.name = songName_R;
                                songItem_R.hand = "Right";

                                updateBmp(rawMidi_R, songItem_R);
                                list_R.Add(songItem_R);

                                songItem.name = songName;
                                songItem.hand = "Leaf";

                                // songItem.clip = clipFile;
                                // SongItemEditor.UpdateBpm(rawMidi, songItem);
                                updateBmp(rawMidi, songItem);
                                list_L.Add(songItem);

                            }
                        }
                    }



                }
            }
            //LR
            foreach (string asset_LR in files)
            {
                if (asset_LR.EndsWith(".mid"))
                {
                    var detectedBpm_LR = -1;
                    var bpmString_LR = "";

                    var rawMidi_LR = MidiFile.Read(asset_LR);

                    SongItem songItem_LR =
                        ScriptableObject.CreateInstance<SongItem>();

                    if (songItem_LR.bpm == 0 && detectedBpm_LR != -1)
                        songItem_LR.bpm = detectedBpm_LR;

                    string songName_LR;
                    songName_LR = Path.GetFileNameWithoutExtension(asset_LR);

                    songItem_LR.name = songName_LR;
                    songItem_LR.hand = "LeafRight";
                        updateBmp(rawMidi_LR, songItem_LR);
                        list_LR.Add(songItem_LR);

                    
                }
            }

        }

        void updateBmp(MidiFile rawMidi, SongItem songItem)
        {
            if (rawMidi == null)
            {
                Debug.LogError("Cannot find the midi file");
                return;
            }
            songItem.notes.Clear();
            var detectedTempoMap = rawMidi.GetTempoMap();
            var tempoMap =
                songItem.useCurrentBpmMidiImport
                    ? TempoMap
                        .Create(detectedTempoMap.TimeDivision,
                        Tempo.FromBeatsPerMinute(songItem.bpm),
                        detectedTempoMap.TimeSignature.AtTime(0))
                    : detectedTempoMap;

            if (!songItem.useCurrentBpmMidiImport)
                songItem.bpm =
                    (int)rawMidi.GetTempoMap().Tempo.AtTime(0).BeatsPerMinute;

            Debug.Log($"Updating Midi Data {tempoMap.TimeDivision}, {songItem.bpm}bpm");

            int count = 0;
            foreach (var note in rawMidi.GetNotes())
            {
                count++;
                var beat =
                    GetMetricTimeSpanTotal(note
                        .TimeAs<MetricTimeSpan>(tempoMap)) *
                    songItem.bpm /
                    60;
                var beatLength =
                    GetMetricTimeSpanTotal(note
                        .LengthAs<MetricTimeSpan>(tempoMap)) *
                    songItem.bpm /
                    60;

                // Debug.Log(RoundToNearestBeat(beat));
                songItem.notes.Add(new SongItem.MidiNote()
                {
                    noteName = ParseEnum<SongItem.NoteName>(note.NoteName.ToString()),
                    noteOctave = note.Octave,
                    time =
                                     GetMetricTimeSpanTotal(note
                                         .TimeAs<MetricTimeSpan>(tempoMap)),
                    noteLength =
                                     GetMetricTimeSpanTotal(note
                                         .LengthAs<MetricTimeSpan>(tempoMap)),
                    //This two is for recalculating the currect time when the bpm is changed
                    beatIndex = SongItem.RoundToNearestBeat(beat),
                    beatLengthIndex =
                                     SongItem.RoundToNearestBeat(beatLength)
                });

                //Debug.Log(songItem.name + " :  " + note.NoteName);
                //Debug.Log(" :  " + note.Octave);
            }
            Debug.Log(count + " Note(s) detected from Midi file");
        }
        private static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
        private static float GetMetricTimeSpanTotal(MetricTimeSpan ts)
        {
            return (float)ts.TotalMicroseconds / 1000000f;
        }
        IEnumerator downloadsong(string file, SongItem songItem)
        {
            if (songItem.clip != null)
            {
                yield break;
            }
            WWW www = new WWW(file);
            yield return www;
            var clip = www.GetAudioClip();
            Debug.Log(file);
            Debug.Log(www);
            songItem.clip = clip;
            songItem.clip = null;
        }
        IEnumerator Play(string file, SongItem song)
        {
            
          //  yield return StartCoroutine(downloadsong(file, song));

            realnote.resetinit();
            fruitnote.resetinit();
            var manager = GameObject.FindObjectOfType<SongManager>();
            manager.defaultSong = song;
            manager.PlaySong();
            Debug.Log("Play song");
            var isplay = Backgourd.GetComponent<BackgourdControl>();
            isplay.setIsPlay(true);

            yield return new WaitForSeconds(3);
            CountdownController countDown = CountDown.GetComponent<CountdownController>();
            countDown.StartCountDown();
            Songname.SetActive(false);
        }
        public void ClickSelet(int id)
        {
           
            index = id;
            //Path_L = file_L;
            //Path_R = file_R;
            //Path_LR = file_LR;
            Debug.Log("index  " + index);
        }
        public void ClickSetLeaf(int type)
        {
            typehand = type;
            Debug.Log(Path_L);
            Debug.Log(Path_R);
            Debug.Log(Path_LR);
            Debug.Log(typehand);
        }
        public void ClickCenterPlay()
        {

           
            Debug.Log("typehand     :   " + typehand);
            if (typehand==1)
            {
                Client.Instance.PlayMusic(list_L[index].name, typehand);
                ClickPlay(Path_L, list_L[index]);


            }
            if (typehand==0)
            {
                Client.Instance.PlayMusic(list_R[index].name, typehand);
                ClickPlay(Path_R, list_R[index]);
            }
            //Both
            if (typehand == 3)
            {
                Client.Instance.PlayMusic(list_LR[index].name, typehand);
                ClickPlay(Path_LR, list_LR[index]);
            }
            
            


            Songname.SetActive(true);
        }
      
        public void ClickPlay(string file, SongItem song)
        {
            //var countdown= GameObject.FindObjectOfType<CountdownController>();
            
            Stop.SetActive(false);
            StudentUI.SetActive(false);
            SetOnPlay();

            var script = Backgourd.GetComponent<BackgourdControl>();
            script.repos();

            StartCoroutine(loadforLoadImagescript(file, song));
            
            
            foreach (var e in Effects)
            {
                e.SetActive(true);
            }
            
            Triger.SetActive(true);
            clearRefreshUI();
        }
        public IEnumerator loadforLoadImagescript(string file, SongItem song)
        {
            LoadingUI.SetActive(true);
            LoadImageSprite[] o = GameObject.FindObjectsOfType<LoadImageSprite>();
            Debug.Log(list_L[index].name+"-----------");
            var script_Level = Level_List.GetComponent<LevelManager>();
            foreach (var item in o)
            {
                yield return StartCoroutine(item.LoadImg(list_L[index].name, script_Level.NowLevel));
            }
            var script = Backgourd.GetComponent<BackgourdControl>();
            script.Instantiatebackground();
            Debug.Log(o.Length);
            LoadingUI.SetActive(false);
            Dino.SetActive(true);
            StartCoroutine(Play(file, song));
        }
        public void SetOnPlay()
        {
            BGUI.SetActive(false);
            Float.SetActive(true);
           
            Backgourd.SetActive(true);
            if (Client.Instance.stat == "teacher")
            {
                Selection.SetActive(false);
            }

            Debug.Log("SetStart");
        }


        [ContextMenu("TestRefreshUI")]
        public void RefreshUI()
        {
            Debug.Log("RefreshUI---------");
            int i = 0;
            bool first=false;
            var files = Directory.GetFiles(Application.streamingAssetsPath);

            foreach (string asset_Level in files)
            {

                Debug.Log(asset_Level);

                string Levelname;
                Levelname = Path.GetFileNameWithoutExtension(asset_Level);
                Debug.Log(Levelname);

                var selectLevel = Level_List.GetComponent<LevelManager>();

                Debug.Log(selectLevel.NowLevel);
                if (selectLevel.NowLevel == Levelname)
                {
                    foreach (var target in list_L)
                    {
                        i++;
                        var file_Song = Directory.GetFiles(Application.streamingAssetsPath + "/" + Levelname);
                        foreach (string asset_S in file_Song)
                        {
                            string songName;
                            songName = Path.GetFileNameWithoutExtension(asset_S);
                            if (songName == target.name)
                            {
                                var read = transform.GetComponent<ReadjsonSongInfo>();

                                var item = Instantiate(PrefabSelectSong, container);
                                var script = item.GetComponent<SongItemUI>();

                                foreach (var data in read.items)
                                {
                                    if (data.songname == target.name)
                                    {
                                        script.T1.SetText(data.textlesson);
                                        script.T2.SetText(data.textpractice);
                                    }
                                }
                                script.T3.SetText(target.name);
                                script.Setindex(i - 1);


                                var p = script.button.GetComponent<Playlist>();
                                p.Setmynumberlist(i - 1);

                                

                                item.transform.localScale = Vector3.one;
                                item.name = target.name;
                                if (!first)
                                {
                                    script.Select();
                                    ClickSelet(i - 1);
                                    var scriptSelection = Selection.GetComponent<SelectionManager>();
                                    scriptSelection.SetDetail(item.name);
                                    first = true;
                                }



                            }
                        }

                    }
                }
            }


          
                //Debug.Log(item.transform.GetChild(0).GetComponent<TMP_Text>().text);

                //var textname = item.transform.GetChild(0).GetChild(4).GetComponent<TMP_Text>();
                //textname.text = target.name;
                //Debug.Log(textname);
                //var c = item.transform.GetChild(1).GetComponent<Playlist>();
                //c.Setmynumberlist(i - 1);

                //var files = Directory.GetFiles(Application.streamingAssetsPath+"/"+ target.name+"/Both");
                //var files_L = Directory.GetFiles(Application.streamingAssetsPath + "/" + target.name+"/L");
                //var files_R = Directory.GetFiles(Application.streamingAssetsPath + "/" + target.name+ "/R");
                //foreach (string asset in files_L)
                //{
                //    if (asset.EndsWith(".wav"))
                //    {
                //        if (Path.GetFileNameWithoutExtension(asset) == item.name+"_L")
                //        {
                //            foreach (string asset_R in files_R)
                //            {
                //                if (asset_R.EndsWith(".wav"))
                //                {
                //                    if (Path.GetFileNameWithoutExtension(asset_R) == item.name + "_R")
                //                    {
                //                        foreach (string asset_LR in files)
                //                        {
                //                            if (asset_LR.EndsWith(".wav"))
                //                            {
                //                                if (Path.GetFileNameWithoutExtension(asset_LR) == item.name)
                //                                {
                //                                    p.SetPath(asset, asset_R, asset_LR);
                //                                    if (i == 1)
                //                                    {
                //                                        ClickSelet( i - 1);
                //                                        var scriptSelection = Selection.GetComponent<SelectionManager>();
                //                                        scriptSelection.SetDetail(item.name);
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                                
                //        }
                //    }
                //}
            
        }
        public void clearRefreshUI()
        {
            //var script = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<SongItemUI>();
            //Debug.Log(transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).name);
            //script.click();
            foreach (Transform child in container)
            {
                GameObject.Destroy(child.gameObject);
            }
            
        }
        public void Exit()
        {
            Application.Quit();
        }
      
    }
}
