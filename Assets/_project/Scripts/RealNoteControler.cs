using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using RhythmGameStarter;
public class RealNoteControler : MonoBehaviour
{
    public Transform container;
    public Transform A, B;
    public float targetp;
    public float p;
    public Vector3 lastnote;
    public TrackManager track;
    public SongManager song;
    public bool Isinit;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        p = Mathf.Lerp(p, targetp, 16 * Time.deltaTime);
        container.localPosition = math.Lerp(B.localPosition, lastnote, p);
    }
    public void fill(float progress)
    {
        if (!Isinit) {
            init();
        }
        targetp = progress;
        if (progress >= 1)
        {
            Debug.Log("End");
        }
    }
    public void init()
    {
        p = -10;
        gameObject.SetActive(true);
        //var list = new List<SongItem.MidiNote>();
        //foreach (var t in track.tracks)
        //{
        //    list.AddRange(t.allNotes);
        //}
        //var last = list.OrderBy(x => x.time).Last();
        var time = song.currentSongItem.length;
        var beatUnit = time / song.secPerBeat;
        var y = beatUnit * track.beatSize ;
        lastnote = B.localPosition;
        lastnote.x -= y;
        Debug.Log("--Y--  "+y);
        Debug.Log(gameObject.name);
        Isinit = true;
    }
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(B.parent.TransformPoint(lastnote), B.parent.TransformPoint(B.localPosition));
    }
    public void resetinit()
    {
        Isinit = false;
    }
}
