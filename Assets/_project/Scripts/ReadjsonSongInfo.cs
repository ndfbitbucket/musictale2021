using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ReadjsonSongInfo : MonoBehaviour
{
    public List<SongInfo> items;
    string path;
    string jsonString;

    // Use this for initialization
    void Start()
    {
       
    }
    public void readj(string level)
    {
        var Level = GameObject.FindObjectOfType<LevelManager>();
        var files = Directory.GetFiles(Application.streamingAssetsPath +"/"+ level);
        foreach (string asset in files)
        {
            string songName;
            songName = Path.GetFileNameWithoutExtension(asset);
            path = Application.dataPath + "/StreamingAssets/" + level + "/" + songName + "/text.json";
            jsonString = File.ReadAllText(path);
            Debug.Log(jsonString);

            SongInfo s = new SongInfo();
            s = JsonUtility.FromJson<SongInfo>(jsonString);

            items.Add(s);
            Debug.Log(s.songname);
            Debug.Log(s.length);
            Debug.Log(s.textlesson);
            Debug.Log(s.textpractice);
        }
    }
    [System.Serializable]
    public class SongInfo
    {
        public string songname;
        public float length;
        public string textlesson;
        public string textpractice;
        public string toJson()
        {
            return JsonUtility.ToJson(this);
        }
    }


    //public void ReadInfo()
    //{
    //    var files = Directory.GetFiles(Application.streamingAssetsPath);
    //    foreach (string asset in files)
    //    {
    //        string songName;
    //        songName = Path.GetFileNameWithoutExtension(asset);
    //        string strOutput = JsonUtility.ToJson(mysong);
    //        File.WriteAllText(Application.dataPath + "/StreamingAssets/" + songName + "/text.txt", strOutput);

    //    }
    //}
}
