using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgourdControl : MonoBehaviour
{
    public bool IsPlay;
    public Transform Layer1;
    public Transform Layer2;
    public Transform Layer3;
    public Transform Layer4;
    public float Layer1HorizontalLength, Layer2HorizontalLength, Layer3HorizontalLength, Layer4HorizontalLength;
    public float lengthOneSet;
    public List<GameObject> listIntant;
    //private Vector3 StartPosition;
    public GameObject SetLayer2, SetLayer3, SetLayer4;
    // Start is called before the first frame update
    void Start()
    {





    }

    // Update is called once per frame
    void Update()
    {
        if (IsPlay)
        {
            foreach (Transform inLayer1 in Layer1)
            {
                inLayer1.Translate(Layer1HorizontalLength, 0, 0);
            }
            foreach (Transform inLayer2 in Layer2)
            {
                inLayer2.Translate(Layer2HorizontalLength, 0, 0);
            }
            foreach (Transform inLayer3 in Layer3)
            {
                inLayer3.Translate(Layer3HorizontalLength, 0, 0);
            }
            foreach (Transform inLayer4 in Layer4)
            {
                inLayer4.Translate(Layer4HorizontalLength, 0, 0);
            }



        }


    }
    public void setIsPlay(bool play)
    {
        IsPlay = play;
    }
    public void Instantiatebackground() {

        for (int i = 0; i < 2; i++)
        {
            var bg2 = Instantiate(SetLayer2, Layer2);
            bg2.name = "Layer2";
            var script = bg2.GetComponent<UpdateLoopBG>();
            script.outOfMap = script.outOfMap - (30 * (i + 1));
            script.myset = (i + 2);
            script.frist = false;
            var map2 = bg2.transform.GetChild(0);
            map2.position = new Vector3(map2.position.x + (30 * (i + 1)), map2.position.y, map2.position.z);
            listIntant.Add(bg2);

            var bg3 = Instantiate(SetLayer3, Layer3);
            bg3.name = "Layer3";
            var script2 = bg3.GetComponent<UpdateLoopBG>();
            script2.outOfMap = script2.outOfMap - (30 * (i + 1));
            script2.myset = (i + 2);
            script2.frist = false;
            var map3 = bg3.transform.GetChild(0);
            map3.position = new Vector3(map3.position.x + (30 * (i + 1)), map3.position.y, map3.position.z);
            var map3d2 = bg3.transform.GetChild(1);
            map3d2.position = new Vector3(map3d2.position.x + (30 * (i + 1)), map3d2.position.y, map3d2.position.z);
            listIntant.Add(bg3);


            var bg4 = Instantiate(SetLayer4, Layer4);
            bg4.name = "Layer4";
            var script3 = bg4.GetComponent<UpdateLoopBG>();
            script3.outOfMap = script3.outOfMap - (30 * (i + 1));
            script3.myset = (i + 2);

            script3.frist = false;

            var map4 = bg4.transform.GetChild(0);
            map4.position = new Vector3(map4.position.x + (30 * (i + 1)), map4.position.y, map4.position.z);
            listIntant.Add(bg4);


        }

    }
    public void repos()
    {
        for (int i = 0; i < Layer1.childCount; i++)
        {

            Layer1.GetChild(0).transform.position = Vector3.zero;

        }
        for (int i = 0; i < Layer2.childCount; i++)
        {

            Layer2.GetChild(0).transform.position = Vector3.zero;

        }
        for (int i = 0; i < Layer3.childCount; i++)
        {

            Layer3.GetChild(0).transform.position = Vector3.zero;

        }
        for (int i = 0; i < Layer4.childCount; i++)
        {

            Layer4.GetChild(0).transform.position = Vector3.zero;


        }



    }
    public void clearIntant(){

        foreach (var child in listIntant)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}
