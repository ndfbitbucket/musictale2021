using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class BtnPlayVDO : MonoBehaviour
{
    public Button yourButton;
    public int VDOnumber;
    public TextMeshProUGUI T1;
    // Start is called before the first frame update
    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        Debug.Log(VDOnumber);
        var game = GameObject.FindObjectOfType<MyVDOList>();

       // game.GetComponent<MyVDOList>().SetClipVDO(VDOnumber);
        game.GetComponent<MyVDOList>().BtnPlay(VDOnumber);
        
    }
}
