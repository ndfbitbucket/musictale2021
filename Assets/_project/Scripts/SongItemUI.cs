using RhythmGameStarter;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class SongItemUI : MonoBehaviour
{
    public TextMeshProUGUI T1, T2, T3;
    public Image bg, icon;
    public bool select;
    public Color color;
    public Button button;
    public int indexSong;
    // Start is called before the first frame update
    void Start()
    {
      
    }
    public void click()
    {
        var list = transform.parent.GetComponentsInChildren<SongItemUI>();
        foreach (var item in list)
        {
            if (item.select)
            {
                item.select = false;
                item.bg.color = Color.white;

            }
        }
        Select();
        var mymidi = transform.parent.parent.parent.parent.GetComponent<MymidiImporter>();
        mymidi.ClickSelet(indexSong);
        var selection = GameObject.FindObjectOfType<SelectionManager>();
        var script = selection.GetComponent<SelectionManager>();
        Debug.Log(gameObject.name);
        script.SetDetail(gameObject.name);
    }
    public void Select()
    {
        select = true;
        bg.color = color;
        


    }
    public void Setindex(int indexs)
    {

        indexSong = indexs;

    }
}
