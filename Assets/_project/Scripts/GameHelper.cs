using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
public class GameHelper : MonoBehaviour
{
    public GameObject UI;
    public GameObject VDOmanager;
    public GameObject myVDOlist;
    public int VDONumber;
  
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void BtnPlay()
    {
        VDOmanager.SetActive(true);
        UI.SetActive(false);
    }
    public void SetClipVDO(int VDOnumber)
    {
        
        myVDOlist.GetComponent<MyVDOList>().PlayVDO(VDOnumber);
    }

}
