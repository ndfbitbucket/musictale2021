using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace RhythmGameStarter
{
    public class SoundManager : MonoBehaviour
    {
        public AudioClip[] Note;
        public AudioSource audioScr;
        // Start is called before the first frame update
        void Start()
        {
            audioScr = GetComponent<AudioSource>();
        }
        [ContextMenu("Test")]
        public void playSoundNote(int n)
        {
            audioScr.PlayOneShot(Note[n]);
        }

    }
}
