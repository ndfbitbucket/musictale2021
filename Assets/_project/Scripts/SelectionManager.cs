﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class SelectionManager : MonoBehaviour
{
    public TextMeshProUGUI T1, T2, T3;
    public Image Detail;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }
 public void SetDetail(string songname)
    {
        
        var read = GameObject.FindObjectOfType<ReadjsonSongInfo>();
        
        foreach (var item in read.items)
        {

            if (item.songname == songname)
            {
                T1.SetText(item.textlesson);
                T2.SetText(item.textpractice);
            }
        }

    }
    public void SetT3(string Levelname)
    {
        T3.SetText(Levelname);
    }
}

