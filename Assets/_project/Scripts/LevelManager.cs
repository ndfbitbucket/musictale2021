using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public string NowLevel;
    public SelectionManager manager;
    // Start is called before the first frame update
    void Start()
    {
        NowLevel= "Level1";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetLevel(string Level)
    {
        NowLevel = Level;

    }
    public void SetTextNameLevel(string Level)
    {
        manager.SetT3(Level);

    }

}
