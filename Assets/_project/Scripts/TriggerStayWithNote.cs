using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RhythmGameStarter;

public class TriggerStayWithNote : MonoBehaviour
{
    public SongManager song;
    public bool eat, eat2;
    public string Dino_name;
    public float delay;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "MyNote")
        {
            //Debug.Log("why wont you work ;_;");
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
       // Debug.Log("Stay");
        if (other.gameObject.tag == "MyNote")
        {
            if (Dino_name == "Dino1")
            {
                eat = song.dino1_eat;
            }
            if (Dino_name == "Dino11")
            {
                eat2 = song.dino11_eat;
            }

            if (eat)
            {
                Destroy(other.gameObject, delay);
            }
            if (eat2)
            {
                Destroy(other.gameObject, delay);
            }

        }
    }
}
